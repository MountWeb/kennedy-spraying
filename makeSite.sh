#!/bin/bash

rm -r ext/*

cp index.html ext/
mkdir ext/img
cp img/*.webp ext/img/

elm make src/Home.elm --optimize --output=ext/js/Home.max.js
uglifyjs ext/js/Home.max.js --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output=ext/js/Home.js
