module Home exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Ui as Ui


main =
    Element.layout [] view



-- View


view =
    column
        [ width fill
        , height fill
        ]
        [ Ui.topbar
        , Ui.annoucementItem
            ( annoucementTitle
            , annoucementExtra
            )
        , Ui.twoPane
            { title = aboutTitle
            , mainPiece = aboutMainPiece
            , content = aboutContent
            , pictureUrl = aboutPictureUrl
            , pictureAlt = aboutPictureAlt
            }
        , Ui.twoText
            { title = servicesTitle
            , textOne = servicesAgSpray
            , textTwo = servicesFoSpray
            }
        , Ui.infoTable
            { title = tableTitle
            , description = servicesYouNeed
            , table = servicesProvided
            }
        , Ui.imageTable
            { title = districtsTitle
            , imageSrc = districtsImage
            , subTitle = districtsSubTitle
            , items = districtsItems
            }
        , Ui.bottomMatter
        ]



-- Content


annoucementTitle : String
annoucementTitle =
    "BUSINESS AS USUAL DESPITE COVID19"


annoucementExtra : String
annoucementExtra =
    "Ground Application of Herbicide, pesticide and fertilizer throughout the South East of South Australia and Western Districts of Victoria"


aboutTitle : String
aboutTitle =
    "About Us"


aboutMainPiece : String
aboutMainPiece =
    """
We are a well established agricultural ground spraying operation based in Mount Gambier.
                  """


aboutContent : String
aboutContent =
    """
     Our mission is to create success and
     profitability for our clients with a service
     dedicated to the needs of our customers.
     We endeavour to create productivity while
     maintaining environmental integrity.

     We stay educated in our ever changing industry,
     keeping up to date with new products and
     technology.
    """


aboutPictureUrl : String
aboutPictureUrl =
    "img/manhimself.webp"


aboutPictureAlt : String
aboutPictureAlt =
    "It's kennedy!"


servicesTitle : String
servicesTitle =
    "Services"


servicesAgSpray : Element msg
servicesAgSpray =
    Ui.basicParagraph
        { title = "Agricultural Spraying"
        , content = """
                    From pest control to fertilisers, we’re a team who will get
                    the job done right the first time, every time. We work
                    efficiently and are able to answer any questions or address
                    concerns before we get to work so that you feel at ease
                    throughout the entire process.
                    """
        }


servicesFoSpray : Element msg
servicesFoSpray =
    Ui.basicParagraph
        { title = "Forestry Spraying"
        , content = """
                     We provide forestry spraying services for a number of
                     different purposes on both privately owned and public
                     plantations. We have years of experience in the forestry
                     industry and are happy to take on any unique tasks that
                     come our way.
                    """
        }


tableTitle : String
tableTitle =
    "On your Farm"


servicesYouNeed : String
servicesYouNeed =
    """
     We offer a list of essential services that will help your farm be more
     profitable.
    """


servicesProvided : List Ui.WhichRow
servicesProvided =
    [ Ui.Row1 "◆ Pre-Sow Spray outs"
    , Ui.Row2 "◆ Pre-em and Post-em herbicide & insecticide"
    , Ui.Row1 "◆ Selective herbicide spraying on crops and pastures"
    , Ui.Row2 "◆ Insect control for grubs, Lucerne flea, grasshoppers etc"
    , Ui.Row1 "◆ Plant growth regulators"
    , Ui.Row2 "◆ Liquid fertilizer"
    , Ui.Row1 "◆ Biologicals"
    , Ui.Row2 "◆ Fungicides"
    , Ui.Row1 "◆ Snail and Slug bating"
    , Ui.Row2 "◆ Broadleaf weed removal"
    ]


districtsTitle : String
districtsTitle =
    "Our Service districts"


districtsImage : String
districtsImage =
    "img/service-area.webp"


districtsSubTitle : String
districtsSubTitle =
    "Our service districts include:"


districtsItems : List Ui.WhichRow
districtsItems =
    [ Ui.Row1 "Casterton"
    , Ui.Row2 "Edenhope"
    , Ui.Row3 "Glencoe"
    , Ui.Row1 "Hamilton"
    , Ui.Row2 "Hatherleigh"
    , Ui.Row3 "Heywood"
    , Ui.Row1 "Kalangadoo"
    , Ui.Row2 "Kongorong"
    , Ui.Row3 "Millicent"
    , Ui.Row1 "Mount"
    , Ui.Row2 "Gambier"
    , Ui.Row3 "Mumbannar"
    , Ui.Row1 "Nangwarry"
    , Ui.Row2 "Nelson"
    , Ui.Row3 "Portland"
    , Ui.Row1 "Port MacDonnell"
    , Ui.Row2 "Rendelsham"
    , Ui.Row3 "Strathdownie"
    , Ui.Row1 "Tantanoola"
    , Ui.Row2 "Tarpeena"
    ]
