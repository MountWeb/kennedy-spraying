module Ui exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font



-- Constants


pageFont : Attribute msg
pageFont =
    Font.family
        [ Font.typeface "Manrope"
        , Font.sansSerif
        ]


titleFont : Attribute msg
titleFont =
    Font.family
        [ Font.typeface "Roboto"
        , Font.sansSerif
        ]


colours :
    { white : Element.Color
    , black : Element.Color
    , red : Element.Color
    , green : Element.Color
    , greenparent : Element.Color
    , lightGreen : Element.Color
    , offwhite : Element.Color
    }
colours =
    { white = rgb255 255 255 255
    , black = rgb255 0 0 0
    , red = rgb255 238 28 37
    , green = rgb255 3 140 125
    , greenparent = rgba255 3 140 125 0.7
    , lightGreen = rgb255 45 173 148
    , offwhite = rgb255 255 240 255
    }


scale : Int -> Float
scale =
    Element.modular 16 1.25


sU : Int
sU =
    110



-- Components


topbar : Element msg
topbar =
    column
        [ padding 10
        , height <| px sU
        , Background.color colours.green
        , width fill
        , centerX
        , pageFont
        , centerY
        ]
        [ el
            [ Font.bold
            , centerX
            , titleFont
            , Font.color colours.white
            , paddingEach
                { top = 12
                , right = 0
                , bottom = 24
                , left = 0
                }
            ]
            (text "Kennedy Spraying Services")
        , row
            [ centerX, spacing 5 ]
            [ el [ alignRight ] (menuItem "Home")
            , el [ alignRight ] (menuItem "About")
            , el [ alignRight ] (menuItem "Services")
            , el [ alignRight ] (menuItem "Parts")
            , el [ alignRight ] (menuItem "Gallery")
            ]
        ]


menuItem : String -> Element msg
menuItem menuText =
    el
        [ Font.color colours.white
        , padding 5
        ]
        (link []
            { url = menuText ++ ".elm"
            , label = text menuText
            }
        )


annoucementItem : ( String, String ) -> Element msg
annoucementItem ( title, extra ) =
    image
        [ centerX
        , height <| px (sU * 6)
        , width fill
        , clipY
        , pageFont
        , inFront
            (el
                [ Background.color colours.greenparent
                , height fill
                , width fill
                , centerX
                , centerY
                , inFront
                    (pictureTitle
                        ( title, extra )
                    )
                ]
                none
            )
        ]
        { description = "ag sprayer"
        , src = "img/ag-spray.webp"
        }


pictureTitle : ( String, String ) -> Element msg
pictureTitle ( title, extra ) =
    row [ centerY ]
        [ el [ width (fillPortion 2) ] none
        , column
            [ centerY
            , centerX
            , Font.color colours.white
            , width (fillPortion 8)
            , spacing (round (scale 1))
            , titleFont
            ]
            [ el
                [ Font.size (round (scale 6))
                , centerY
                , centerX
                ]
                (text title)
            , paragraph
                [ Font.size (round (scale 3))
                , centerY
                , centerX
                , width fill
                , Font.center
                ]
                [ text extra ]
            ]
        , el [ width (fillPortion 2) ] none
        ]


twoPane :
    { title : String
    , mainPiece : String
    , content : String
    , pictureUrl : String
    , pictureAlt : String
    }
    -> Element msg
twoPane { title, mainPiece, content, pictureUrl, pictureAlt } =
    row
        [ paddingXY 0 30
        , height <| px (sU * 4)
        , centerY
        , pageFont
        ]
        [ el [ width (fillPortion 2) ] none
        , column [ width (fillPortion 14) ]
            [ el
                -- Title
                [ Font.size (round (scale 5))
                , Font.bold
                , Font.color colours.green
                , titleFont
                ]
                (text title)
            , row
                [ width (fillPortion 1)
                , spacing 15
                ]
                [ column
                    [ width (fillPortion 1)
                    , spacing 20
                    , paddingXY 0 15
                    ]
                    [ paragraph
                        -- Main Piece
                        [ Font.size (round (scale 2))
                        , spacing 10
                        ]
                        [ el
                            [ Font.bold
                            , Font.color colours.lightGreen
                            ]
                            (text mainPiece)
                        ]
                    , paragraph
                        [ Font.size (round (scale 1))
                        , spacing 10
                        ]
                        [ el [] (text content) ]
                    ]
                , image [ width (fillPortion 1) ]
                    { description = pictureAlt
                    , src = "img/manhimself.webp"
                    }
                ]
            ]
        , el [ width (fillPortion 2) ] none
        ]


twoText :
    { title : String
    , textOne : Element msg
    , textTwo : Element msg
    }
    -> Element msg
twoText { title, textOne, textTwo } =
    row
        [ paddingXY 0 30
        , height <| px (sU * 4)
        , pageFont
        ]
        [ el [ width (fillPortion 2) ] none
        , column
            [ width (fillPortion 14)
            , spacing 20
            ]
            [ el
                [ Font.bold
                , Font.size
                    (round (scale 5))
                , Font.color colours.green
                , titleFont
                ]
                (text title)
            , row
                [ spacing 15
                , centerY
                ]
                [ textOne
                , textTwo
                ]
            ]
        , el [ width (fillPortion 2) ] none
        ]


basicParagraph :
    { title : String
    , content : String
    }
    -> Element msg
basicParagraph { title, content } =
    column [ spacing 15, width fill ]
        [ el
            -- Title
            [ Font.bold
            , Font.size (round (scale 2))
            , Font.color colours.lightGreen
            ]
            (text title)
        , paragraph
            [ Font.size (round (scale 1))
            , spacing 10
            ]
            [ text content ]
        ]


infoTable :
    { title : String
    , description : String
    , table : List WhichRow
    }
    -> Element msg
infoTable { title, description, table } =
    row
        [ Background.color colours.green
        , height <| px (4 * sU)
        , width fill
        , paddingXY 0 30
        ]
        [ el [ width (fillPortion 2) ] none
        , column
            [ width (fillPortion 14)
            , height fill
            , Font.color colours.white
            ]
            [ el
                -- Title
                [ Font.bold
                , Font.size
                    (round (scale 5))
                , titleFont
                , paddingEach
                    { top = 0
                    , right = 0
                    , bottom = 48
                    , left = 0
                    }
                ]
                (text title)
            , paragraph
                -- Description
                [ titleFont ]
                [ el
                    []
                    (text description)
                ]
            , row [ pageFont ] <| mapItTwoColumns table
            ]
        , el [ width (fillPortion 2) ] none
        ]


mapItTwoColumns : List WhichRow -> List (Element msg)
mapItTwoColumns list =
    [ column [ alignTop ] <| List.map tableMaker1 list
    , column [ alignTop ] <| List.map tableMaker2 list
    ]


imageTable :
    { title : String
    , imageSrc : String
    , subTitle : String
    , items : List WhichRow
    }
    -> Element msg
imageTable { title, imageSrc, subTitle, items } =
    row [ height <| px (sU * 5), centerX ]
        [ el [ width <| fillPortion 2 ] none
        , column
            [ width <| fillPortion 14
            , spacing 20
            , centerX
            ]
            [ el
                [ Font.bold
                , Font.size (round (scale 5))
                , Font.color colours.green
                , titleFont
                ]
                (text "Our Service Districts")
            , row
                [ centerX
                , width fill
                , spacing 20
                , pageFont
                , Font.bold
                , Font.color colours.lightGreen
                , Font.size <| round <| scale 2
                ]
                [ image []
                    { src = "img/service-area.webp"
                    , description = "google maps amiright"
                    }
                , row [] <|
                    mapItThreeColumns items
                ]
            ]
        , el [ width <| fillPortion 2 ] none
        ]


type WhichRow
    = Row1 String
    | Row2 String
    | Row3 String


mapItThreeColumns : List WhichRow -> List (Element msg)
mapItThreeColumns list =
    [ column [ alignTop ] <| List.map tableMaker1 list
    , column [ alignTop ] <| List.map tableMaker2 list
    , column [ alignTop ] <| List.map tableMaker3 list
    ]


tableMaker1 : WhichRow -> Element msg
tableMaker1 row =
    case row of
        Row1 place ->
            el [ alignLeft, padding 15 ] (text place)

        Row2 place ->
            none

        Row3 place ->
            none


tableMaker2 : WhichRow -> Element msg
tableMaker2 row =
    case row of
        Row1 place ->
            none

        Row2 place ->
            el [ alignLeft, padding 15 ] (text place)

        Row3 place ->
            none


tableMaker3 : WhichRow -> Element msg
tableMaker3 row =
    case row of
        Row1 place ->
            none

        Row2 place ->
            none

        Row3 place ->
            el [ alignLeft, padding 15 ] (text place)


bottomMatter : Element msg
bottomMatter =
    row
        [ Background.color colours.green
        , height <| px sU
        , width fill
        , pageFont
        ]
        [ el [ width <| fillPortion 1 ] none
        , row [ width <| fillPortion 20, centerX, spaceEvenly ]
            [ column
                -- contact us
                [ spacing 10
                , width fill
                , alignTop
                ]
                [ el
                    [ Font.bold
                    , Font.color colours.white
                    , Font.size <| round <| scale 1
                    ]
                    (text "Contact Us")
                , paragraph []
                    [ el
                        [ Font.bold
                        , Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "LANDLINE: ")
                    , el
                        [ Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "08 8725 3380")
                    ]
                , paragraph []
                    [ el
                        [ Font.bold
                        , Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "MOBILE: ")
                    , el
                        [ Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "0408 849 330")
                    ]
                ]
            , column
                -- Hours of operation
                [ width fill
                , spacing 10
                , alignTop
                ]
                [ el
                    [ Font.bold
                    , Font.color colours.white
                    , Font.size <| round <| scale 1
                    ]
                    (text "Hours of operation")
                , paragraph []
                    [ el
                        [ Font.bold
                        , Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "Monday - Friday: ")
                    , el
                        [ Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "8:00 AM – 6:00 PM")
                    ]
                , paragraph []
                    [ el
                        [ Font.bold
                        , Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "Saturday - Sunday: ")
                    , el
                        [ Font.color colours.white
                        , Font.size <| round <| scale -1
                        ]
                        (text "By Appointment Only")
                    ]
                ]
            , column
                -- Address
                [ width fill
                , spacing 10
                , alignTop
                ]
                [ el
                    [ Font.bold
                    , Font.color colours.white
                    , Font.size <| round <| scale 1
                    ]
                    (text "ADDRESS ")
                , el
                    [ Font.color colours.white
                    , Font.size <| round <| scale -1
                    ]
                    (text "40 White Ave, Mount Gambier SA 5290")
                , el
                    [ Font.color colours.white
                    , Font.size <| round <| scale -1
                    ]
                    (text "ABN 55 104 120 550")
                ]
            ]
        , el [ width <| fillPortion 1 ] none
        ]
